# qssi-user 10 QKQ1.200127.002 29.14.53.23_20210119 release-keys
- manufacturer: 
- platform: konakonakonakona
- codename: ASUS_I002D
- flavor: qssi-user
- release: 10
- id: QKQ1.200127.002
- incremental: 29.14.53.23_20210119
- tags: release-keys
- fingerprint: asus/WW_I002D/ASUS_I002D:10/QKQ1.200127.002/29.14.53.23_20210119:user/release-keys
asus/EU_I002D/ASUS_I002D:10/QKQ1.200127.002/29.14.53.23_20210119:user/release-keys
asus/RU_I002D/ASUS_I002D:10/QKQ1.200127.002/29.14.53.23_20210119:user/release-keys
asus/RU_I002D/ASUS_I002D:10/QKQ1.200127.002/29.14.53.23_20210119:user/release-keys
- is_ab: true
- brand: asus
- branch: qssi-user-10-QKQ1.200127.002-29.14.53.23_20210119-release-keys
- repo: asus_asus_i002d_dump
